function konversiMenit(menit) {
  let cekMenit = Math.floor(menit / 60);
  let sisa = menit - cekMenit * 60;

  if (sisa < 10) {
    return cekMenit + ":0" + sisa;
  } else {
    return cekMenit + ":" + sisa;
  }

  // you can only write your code here!
}

// TEST CASES
console.log(konversiMenit(63)); // 1:03
console.log(konversiMenit(124)); // 2:04
console.log(konversiMenit(53)); // 0:53
console.log(konversiMenit(88)); // 1:28
console.log(konversiMenit(120)); // 2:00
